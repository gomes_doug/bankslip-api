# Bankslip API
Bankslip API é uma API REST para geração de boletos quer será consumido por um módulo de um sistema
de gestão financeira de microempresas. Essa API pode criar um boleto, buscar todos os boletos
ou um boleto específico pelo seu id ou atualizar seu status para pago ou cancelado.

### Funções
- Criar um boleto
- Buscar todos os boletos
- Buscar um boleto pelo seu id
- Atualizar o status para pago ou cancelado

### Instruções
**OBS:** Necessário estar na raiz do projeto. 

Comando para rodar o projeto:

```sh
$ mvn spring-boot:run
```

Comando para dar build no projeto:

```sh
$ mvn clean install
```

Comando para rodar os testes do projeto:

```sh
$ mvn test
```

Comando para realizar o deploy do artefato:

```sh
$ mvn deploy
```

### Documentação da API
Quando o projeto estiver em execução, você poderá visualizar a documentação da API com todos os endpoints em
* (http://localhost:8080/swagger-ui.html)
