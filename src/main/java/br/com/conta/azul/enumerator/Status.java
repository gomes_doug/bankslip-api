package br.com.conta.azul.enumerator;

public enum Status {

	PENDING("PENDING"), PAID("PAID"), CANCELED("CANCELED");

	private String name;

	private Status(String name) {
        this.name= name;
    }

	public String getName() {
		return name;
	}
	
	public static boolean contains(String status) {

	    for (Status c : Status.values()) {
	        if (c.name().equals(status)) {
	            return true;
	        }
	    }
	    
	    return false;
	}
}
