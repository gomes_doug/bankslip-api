package br.com.conta.azul.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.conta.azul.domain.model.Bankslip;

@Repository
public interface BankslipRepository extends JpaRepository<Bankslip, String>{
	
	public Bankslip findById(String id);

}
