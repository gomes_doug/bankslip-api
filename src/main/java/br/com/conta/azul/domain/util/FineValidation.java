package br.com.conta.azul.domain.util;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class FineValidation {

	@Value("${percent.fine.until.ten.days:0.5}")
	private char[] percentFineUntilTenDays;
	
	@Value("${percent.fine.above.ten.days:1}")
	private char[] percentFineAboveTenDays;
	
	public BigDecimal validateFine(Date dueDate, BigDecimal totalInCents) {
		
		if(dueDate.before(new Date()))
			return null; 
		if(dueDate.before(addTenDays(new Date())))
			return untilTenDays(totalInCents);
		else
			return aboveTenDays(totalInCents);
	}

	private BigDecimal untilTenDays(BigDecimal totalInCents) {
		return totalInCents.multiply(new BigDecimal(percentFineUntilTenDays)).divide(new BigDecimal(100));
	}
	
	private BigDecimal aboveTenDays(BigDecimal totalInCents) {
		return totalInCents.multiply(new BigDecimal(percentFineAboveTenDays)).divide(new BigDecimal(100));
	}
	
	private Date addTenDays(Date dueDate) {
		Calendar c = Calendar.getInstance(); 
		c.setTime(dueDate); 
		c.add(Calendar.DATE, 10);
		return c.getTime();
	}
}
