package br.com.conta.azul.domain.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import br.com.conta.azul.domain.model.Bankslip;
import br.com.conta.azul.domain.repository.BankslipRepository;
import br.com.conta.azul.domain.util.FineValidation;
import br.com.conta.azul.enumerator.Status;
import br.com.conta.azul.exception.BusinessException;
import br.com.conta.azul.exception.OperationNotSupportedException;
import br.com.conta.azul.exception.UUIDValidatorException;
import br.com.conta.azul.web.utils.UUIDValidator;

@Service
public class BankslipService {

	@Autowired
	private BankslipRepository bankslipRepository;

	@Autowired
	private FineValidation fineValidation;
	
	@Autowired
	private UUIDValidator uuidValidator;

	@Transactional
	public Bankslip create(Bankslip bankslip) {
		if(bankslip != null) 
			return bankslipRepository.save(bankslip);
		
		throw new BusinessException("The bankslip object can not be null.");
	}

	public List<Bankslip> findAll() {
		return bankslipRepository.findAll();
	}

	public Bankslip findById(String id) {
		
		if(!uuidValidator.validate(id)) {
			throw new UUIDValidatorException();
		}
		
		Bankslip bankslip = bankslipRepository.findById(id);

		if (bankslip == null) 
			throw new EmptyResultDataAccessException(1);
		
		bankslip.setFine(fineValidation.validateFine(bankslip.getDueDate(), bankslip.getTotalInCents()));

		return bankslip;
	}
	
	@Transactional
	public Bankslip update(String id, String status) {
		
		if(!uuidValidator.validate(id)) {
			throw new UUIDValidatorException();
		}
		
		if(StringUtils.isEmpty(status) || Status.PENDING.getName().equals(status)) {
			throw new OperationNotSupportedException();
		}
		
		if(!Status.contains(status)) {
			throw new IllegalArgumentException();
		}
		
		Bankslip bankslip = bankslipRepository.findById(id);
		
		if (bankslip == null) 
			throw new EmptyResultDataAccessException(1);
		
		bankslip.setStatus(Status.valueOf(status));
		return bankslipRepository.save(bankslip);
	}
}
