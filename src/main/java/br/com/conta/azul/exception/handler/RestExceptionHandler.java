package br.com.conta.azul.exception.handler;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import br.com.conta.azul.exception.OperationNotSupportedException;
import br.com.conta.azul.exception.UUIDValidatorException;

@RestControllerAdvice
public class RestExceptionHandler {
	
	@ExceptionHandler({Exception.class})
	protected ResponseEntity<Object> exception(Exception ex) {
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Unexpected error: " + ex.getMessage());
	}
	
	@ExceptionHandler(HttpMessageNotReadableException.class)
	protected ResponseEntity<Object> httpMessageNotReadableException(HttpMessageNotReadableException ex) {
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Bankslip not provided in the request body.");
	}
	
	@ExceptionHandler(UUIDValidatorException.class)
	public ResponseEntity<Object> uUIDValidatorException(UUIDValidatorException ex, WebRequest request) {
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(" Invalid id provided - it must be a valid UUID.");
	}
	
	@ExceptionHandler(OperationNotSupportedException.class)
	public ResponseEntity<Object> operationNotSupportedException(OperationNotSupportedException ex, WebRequest request) {
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("The operation to set as pending is not allowed or status is null.");
	}
	
	@ExceptionHandler(DataIntegrityViolationException.class)
	public ResponseEntity<Object> constraintViolationException(DataIntegrityViolationException ex, WebRequest request) {
		return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body("Invalid bankslip provided.");
	}

	@ExceptionHandler(MethodArgumentNotValidException.class)
	protected ResponseEntity<Object> methodArgumentNotValidException(MethodArgumentNotValidException ex) {
		return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body("Invalid bankslip provided.");
	}
	
	@ExceptionHandler(IllegalArgumentException.class)
	protected ResponseEntity<Object> illegalArgumentException(IllegalArgumentException ex) {
		return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body("Invalid bankslip provided.");
	}
	
	@ExceptionHandler({ EmptyResultDataAccessException.class })
	public ResponseEntity<Object> emptyResultDataAccessException(EmptyResultDataAccessException ex,
			WebRequest request) {
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Bankslip not found with the specified id.");
	}
}
