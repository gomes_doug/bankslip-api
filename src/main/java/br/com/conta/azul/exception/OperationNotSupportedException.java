package br.com.conta.azul.exception;

public class OperationNotSupportedException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public OperationNotSupportedException() {
		super();
	}
}
