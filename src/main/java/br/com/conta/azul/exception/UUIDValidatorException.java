package br.com.conta.azul.exception;

public class UUIDValidatorException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public UUIDValidatorException() {
		super();
	}
}
