package br.com.conta.azul.web.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.conta.azul.web.dto.BankslipDTO;
import br.com.conta.azul.web.facade.BankslipFacade;

@RestController
@RequestMapping("/rest")
public class BankslipController {
	
	@Autowired
	private BankslipFacade bankslipFacade;
	
	@PostMapping("bankslips")
	public ResponseEntity<BankslipDTO> create(@Valid @RequestBody BankslipDTO bankslip) {
		BankslipDTO createdBankslip = bankslipFacade.create(bankslip);
		return ResponseEntity.status(HttpStatus.CREATED).body(createdBankslip);
	}
	
	@GetMapping("bankslips/")
	public ResponseEntity<List<BankslipDTO>> findAll() {
		List<BankslipDTO> bankslipsDTO = bankslipFacade.findAll();
		return ResponseEntity.status(HttpStatus.OK).body(bankslipsDTO);
	}
	
	@GetMapping("bankslips/{id}")
	public ResponseEntity<BankslipDTO> details(@PathVariable("id") String id) {
		BankslipDTO bankslip = bankslipFacade.findById(id);
		return ResponseEntity.status(HttpStatus.OK).body(bankslip);
	}
	
	@PutMapping("bankslips/{id}")
	public ResponseEntity<BankslipDTO> update(@PathVariable("id") String id, @RequestBody String status) {
		BankslipDTO bankslip = bankslipFacade.update(id, status);
		return ResponseEntity.status(HttpStatus.OK).body(bankslip);
	}
}
