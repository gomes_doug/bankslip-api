package br.com.conta.azul.web.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class UUIDValidator {

	private Pattern pattern;
	private Matcher matcher;

	private static final String UUID_PATTERN = "^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$";

	public UUIDValidator() {
		this.pattern = Pattern.compile(UUID_PATTERN);
	}

	public boolean validate(final String uuid) {
		
		if(StringUtils.isEmpty(uuid)) {
			return false;
		}
		
		matcher = pattern.matcher(uuid);
		return matcher.matches();
	}
}
