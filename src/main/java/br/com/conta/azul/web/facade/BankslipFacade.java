package br.com.conta.azul.web.facade;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.conta.azul.domain.model.Bankslip;
import br.com.conta.azul.domain.service.BankslipService;
import br.com.conta.azul.exception.BusinessException;
import br.com.conta.azul.web.dto.BankslipDTO;

@Component
public class BankslipFacade {
	
	@Autowired
	private BankslipService banklipService;

	@Autowired
	private ModelMapper modelMapper;
	
	public BankslipDTO create(BankslipDTO bankslipDTO) {

		if(bankslipDTO == null)
			throw new BusinessException("The bankslip object can not be null.");
		
		Bankslip bankslip = modelMapper.map(bankslipDTO, Bankslip.class);
		bankslip = banklipService.create(bankslip);
		return modelMapper.map(bankslip, BankslipDTO.class);
	}
	
	@SuppressWarnings("unchecked")
	public List<BankslipDTO> findAll() {
		List<Bankslip> banklisps = banklipService.findAll();
		return modelMapper.map(banklisps, ArrayList.class);
	}
	
	public BankslipDTO findById(String id) {
		Bankslip bankslip = banklipService.findById(id);
		return modelMapper.map(bankslip, BankslipDTO.class);
	}
	
	public BankslipDTO update(String id, String status) {
		Bankslip bankslip = banklipService.update(id, status);
		return modelMapper.map(bankslip, BankslipDTO.class);
	}
}
