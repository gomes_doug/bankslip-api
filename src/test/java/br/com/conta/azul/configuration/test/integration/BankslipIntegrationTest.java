package br.com.conta.azul.configuration.test.integration;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.conta.azul.configuration.BankslipsApplicationTest;
import br.com.conta.azul.enumerator.Status;
import br.com.conta.azul.web.dto.BankslipDTO;

@AutoConfigureMockMvc
public class BankslipIntegrationTest extends BankslipsApplicationTest {

	@Autowired
	private MockMvc mvc;
	
	@Autowired
	private ObjectMapper objectMapper;

	private JacksonTester<BankslipDTO> bankslip;
	
	@Before
	public void setup() {
		ObjectMapper objectMapper = new ObjectMapper();
		JacksonTester.initFields(this, objectMapper);
	}

	@Test
	public void canCreateANewBankslip() throws IOException, Exception {

		MockHttpServletResponse response = mvc.perform(post("/rest/bankslips")
			.contentType(MediaType.APPLICATION_JSON)
			.content(bankslip.write(newBankslipDTO()).getJson()))
			.andReturn().getResponse();

		assertThat(response.getStatus()).isEqualTo(HttpStatus.CREATED.value());
	}

	@Test
	public void canCreateANewBankslipWithEmptyBody() throws Exception {

		MockHttpServletResponse response = mvc.perform(post("/rest/bankslips")
				.contentType(MediaType.APPLICATION_JSON))
				.andReturn().getResponse();
		
		assertThat(response.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
	}

	@Test
	public void canCreateANewBankslipWithEmptyFields() throws Exception {

		MockHttpServletResponse response = mvc.perform(post("/rest/bankslips")
			.contentType(MediaType.APPLICATION_JSON)
			.content(bankslip.write(new BankslipDTO()).getJson()))
			.andReturn().getResponse();

		assertThat(response.getStatus()).isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY.value());
	}

	@Test
	public void canRetrieveAll() throws IOException, Exception {

		MockHttpServletResponse response = mvc.perform(get("/rest/bankslips/")
			.contentType(MediaType.APPLICATION_JSON))
			.andReturn().getResponse();

		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
	}
	
	@Test
	public void canRetrieveNonExistsId() throws IOException, Exception {

		MockHttpServletResponse response = mvc.perform(get("/rest/bankslips/c249c83f-aeb1-4c6c-920b-fa336b2ad517")
			.contentType(MediaType.APPLICATION_JSON))
			.andReturn().getResponse();

		assertThat(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());
	}
	
	@Test
	public void canRetrieveByInvalidId() throws IOException, Exception {

		MockHttpServletResponse response = mvc.perform(get("/rest/bankslips/1234")
			.contentType(MediaType.APPLICATION_JSON))
			.andReturn().getResponse();

		assertThat(response.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
	}
	
	@Test
	public void canRetrieveById() throws IOException, Exception {

		MockHttpServletResponse responseCreated = mvc.perform(post("/rest/bankslips")
				.contentType(MediaType.APPLICATION_JSON)
				.content(bankslip.write(newBankslipDTO()).getJson()))
				.andReturn().getResponse();
		
		BankslipDTO bankslipDTO = objectMapper.readValue(responseCreated.getContentAsString(), BankslipDTO.class); 
		
		MockHttpServletResponse response = mvc.perform(get("/rest/bankslips/" + bankslipDTO.getId())
			.contentType(MediaType.APPLICATION_JSON))
			.andReturn().getResponse();

		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
	}
	
	
	@Test
	public void canUpdateNonExistisId() throws IOException, Exception {

		BankslipDTO ankslip = newBankslipDTO();
		ankslip.setStatus(Status.PAID);
		
		MockHttpServletResponse response = mvc.perform(put("/rest/bankslips/c249c83f-aeb1-4c6c-920b-fa336b2ad517")
			.contentType(MediaType.APPLICATION_JSON)
			.content(STATUS))
			.andReturn().getResponse();

		assertThat(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());
	}
	
	@Test
	public void canUpdateWithInvalidStatus() throws IOException, Exception {

		MockHttpServletResponse response = mvc.perform(put("/rest/bankslips/c249c83f-aeb1-4c6c-920b-fa336b2ad517")
			.contentType(MediaType.APPLICATION_JSON)
			.content("WAITING"))
			.andReturn().getResponse();

		assertThat(response.getStatus()).isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY.value());
	}
	
	@Test
	public void canUpdate() throws IOException, Exception {

		MockHttpServletResponse responseCreated = mvc.perform(post("/rest/bankslips")
				.contentType(MediaType.APPLICATION_JSON)
				.content(bankslip.write(newBankslipDTO()).getJson()))
				.andReturn().getResponse();
		
		BankslipDTO bankslip = objectMapper.readValue(responseCreated.getContentAsString(), BankslipDTO.class); 
		
		MockHttpServletResponse response = mvc.perform(put("/rest/bankslips/"+ bankslip.getId())
			.contentType(MediaType.APPLICATION_JSON)
			.content(STATUS))
			.andReturn().getResponse();

		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
	}
}
