package br.com.conta.azul.configuration.test.unit;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.conta.azul.configuration.BankslipsApplicationTest;
import br.com.conta.azul.exception.BusinessException;
import br.com.conta.azul.exception.OperationNotSupportedException;
import br.com.conta.azul.exception.UUIDValidatorException;
import br.com.conta.azul.web.facade.BankslipFacade;

public class BankslipFacadeTest extends BankslipsApplicationTest {
	
	@Autowired
	public BankslipFacade bankslipFacade;
	
	@Test(expected = BusinessException.class)
	public void createCanNotBeNull() {
		bankslipFacade.create(null);
	}
	
	@Test(expected = UUIDValidatorException.class)
	public void updateCanUuidBeNull() {
		bankslipFacade.update(null, null);
	}
	
	@Test(expected = OperationNotSupportedException.class)
	public void updateCanStatusNotBeNull() {
		bankslipFacade.update("34f67312-23ce-4e8c-b42e-85baebff9f51", null);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void updateCanStatusNotValid() {
		bankslipFacade.update("34f67312-23ce-4e8c-b42e-85baebff9f51", "WAITING");
	}
}
