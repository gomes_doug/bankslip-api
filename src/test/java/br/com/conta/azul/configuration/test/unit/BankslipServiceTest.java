package br.com.conta.azul.configuration.test.unit;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.conta.azul.configuration.BankslipsApplicationTest;
import br.com.conta.azul.domain.model.Bankslip;
import br.com.conta.azul.domain.service.BankslipService;
import br.com.conta.azul.enumerator.Status;
import br.com.conta.azul.exception.UUIDValidatorException;

public class BankslipServiceTest extends BankslipsApplicationTest {

	@Autowired
	private BankslipService bankslipService;
	
	@Test
	public void canCreateBankslip() {
		Bankslip bankslip = bankslipService.create(newBankslip());
		Assert.assertNotNull(bankslip.getId());
	}
	
	@Test
	public void canUpdateBankslip() {
		
		Bankslip bankslip = bankslipService.create(newBankslip());
		Assert.assertNotNull(bankslip.getId());
		
		bankslip = bankslipService.update(bankslip.getId(), STATUS);
		Assert.assertEquals(Status.PAID, bankslip.getStatus());
	}
	
	@Test(expected=UUIDValidatorException.class)
	public void uuidValidate() {
		Bankslip bankslip = bankslipService.update("12345", STATUS);
		Assert.assertEquals(Status.PAID, bankslip.getStatus());
	}
}
