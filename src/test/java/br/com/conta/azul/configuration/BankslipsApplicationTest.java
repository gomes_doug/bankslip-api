package br.com.conta.azul.configuration;

import java.math.BigDecimal;
import java.util.Date;

import javax.transaction.Transactional;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.com.conta.azul.domain.model.Bankslip;
import br.com.conta.azul.enumerator.Status;
import br.com.conta.azul.web.dto.BankslipDTO;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@Transactional
public abstract class BankslipsApplicationTest {

	protected static final String STATUS = "PAID";
	
	protected Bankslip newBankslip() {
		
		Bankslip bankslip = new Bankslip();
		
		bankslip.setCustomer("Fake Company");
		bankslip.setDueDate(new Date());
		bankslip.setStatus(Status.PENDING);
		bankslip.setTotalInCents(new BigDecimal(2000));
		return bankslip;
	}
	
	protected BankslipDTO newBankslipDTO() {
		
		BankslipDTO bankslip = new BankslipDTO();
		
		bankslip.setCustomer("Fake Company");
		bankslip.setDueDate(new Date());
		bankslip.setStatus(Status.PENDING);
		bankslip.setTotalInCents(new BigDecimal(2000));
		return bankslip;
	}
}
